package edu.uprm.cse.datastructures.cardealer.model;

import edu.uprm.cse.datastructures.cardealer.model.Car;
import edu.uprm.cse.datastructures.cardealer.model.CarComparator;
import edu.uprm.cse.datastructures.cardealer.util.CircularSortedDoublyLinkedList;

/*
 * Singleton class that holds a single instance of a CircularSortedDoublyLinkedList
 * 
 * Has methods to get that instance and to reset the instance (clear it)
 */
public class CarList {

	private static CircularSortedDoublyLinkedList<Car> carList = new CircularSortedDoublyLinkedList<Car>(new CarComparator()); 
	
	public static CircularSortedDoublyLinkedList<Car> getInstance() {
		return carList;
	}
	
	public static void resetCars() {
		carList.clear();
	}
	
}
