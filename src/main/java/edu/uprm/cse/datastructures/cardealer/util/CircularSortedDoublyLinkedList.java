package edu.uprm.cse.datastructures.cardealer.util;

import java.util.Comparator;
import java.util.Iterator;
import java.util.NoSuchElementException;


public class CircularSortedDoublyLinkedList<E> implements SortedList<E> {

	/*
	 * Node class for circular sorted doubly linked list with dummy header
	 * Each node has an element of type 'E' and a previous and a next of type 'Node<E>'
	 * 
	 * There are three constructors, one that doesn't take any parameters and assigns all the fields to null,
	 * one that takes in a single parameter that represents the value to be store in the node's 'element' field,
	 * and one that takes in three parameter: one for the element, one for the previous and one for the next.
	 * 
	 * Each private field has it's setters and getters.
	 * 
	 * There is another method named 'clear()' which sets all the fields of a node to null.
	 */
	@SuppressWarnings("hiding")
	private class Node<E>{
		private E element;
		private Node<E> prev, next;

		public Node(E e, Node<E> prev, Node<E> next) {
			this.element = e;
			this.prev = prev;
			this.next = next;
		}

		public Node(E e) {
			this(e, null, null);
		}

		public Node() { super(); }

		public E getElement() {
			return this.element;
		}
		public void setElement(E e) {
			this.element = e;
		}

		public Node<E> getPrev(){
			return this.prev;
		}
		public void setPrev(Node<E> prev) {
			this.prev = prev;
		}

		public Node<E> getNext(){
			return this.next;
		}
		public void setNext(Node<E> next) {
			this.next = next;
		}

		public E clear() {
			E etr = this.element;
			this.element = null;
			this.prev = this.next = null;
			return etr;
		}

	}

	/*
	 * Iterator class for CSDLLDH ADT
	 * 
	 * The iterator uses a Node<E> to keep track of where it is at.
	 */
	@SuppressWarnings("hiding")
	private class SCDLLDHIterator<E> implements Iterator<E>{

		private Node<E> curr;

		@SuppressWarnings("unchecked")
		public SCDLLDHIterator() {
			this.curr = (Node<E>) header.getNext();
		}

		@Override
		public boolean hasNext() {
			return this.curr.getElement() != null;
		}

		@Override
		public E next() {
			if(this.hasNext()) {
				E itr = this.curr.getElement();
				this.curr = this.curr.getNext();
				return itr;
			}else {
				throw new NoSuchElementException();
			}
		}
	}
	
	

	/*
	 * CSDLLDH private fields
	 * 
	 * One for the size of the structure, one for the header Node<E> of the structure
	 * and one for the comparator.
	 */
	private int currentSize;
	private Node<E> header;

	private Comparator<E> cmp;

	
	/*
	 * CSDLLDH default constructor
	 * 
	 * Only takes in a Comparator as parameter
	 * The size gets assigned to 0 and the header's element to null and both it's previous and next to itself.
	 */
	public CircularSortedDoublyLinkedList(Comparator<E> cmp) {
		this.currentSize = 0;
		this.header = new Node<E>(null, this.header, this.header);
		this.cmp = cmp;
	}

	
	/*
	 * Method return an instance of the CSDLLDH class Iterator
	 */
	@Override
	public Iterator<E> iterator() {
		return new SCDLLDHIterator<E>();
	}

	
	/*
	 * The add method for the CSDLLDH takes one a single parameter to represent the element to be added into the structure
	 * 
	 * First the algorithm checks to see if the structure is empty and if so it just puts the new object in front of the header
	 * and updates the references of the header and the new object.
	 * 
	 * (In both of the following cases the algorithm makes use of an auxiliar method created to add a node, between two other nodes, named 'addBetween')
	 * 
	 * If the structure wasnt empty, it checks if the new object is greater than the current last (and in so, greatest) object in the structure
	 * if this is true then it just adds the new object before the header and in front of the current last object, it then updates the
	 * references of the header, the current last object and the new object.
	 * 
	 * If neather of the above was true then the algorithms checks to see of the new object is lesser than the current first object 
	 * (and in so the lesser in the structure) and of it is then it just adds the new object in between the header and the current first object and updates 
	 * the references of the three. If that isnt the case then it goes on to a while loop that goes on untill the 'temp' node's element is null.
	 * This loop finds the place in wich the new object is supposed to go into to keep the structure sorted. 
	 */
	@Override
	public boolean add(E obj) {
		if(this.isEmpty()) {
			Node<E> newNode = new Node<E>(obj);
			this.header.setPrev(newNode);
			this.header.setNext(newNode);
			newNode.setPrev(this.header);
			newNode.setNext(this.header);
			this.currentSize++;
			return true;
		}else if(cmp.compare(obj, this.header.getPrev().getElement()) >= 0){
			Node<E> newNode = new Node<E>(obj);
			Node<E> temp = this.header.getPrev();
			this.addBetween(newNode, temp, this.header);
			this.currentSize++;
			return true;
		}else {
			Node<E> temp = this.header.getNext();
			Node<E> newNode = new Node<E>(obj);
			if(cmp.compare(newNode.getElement(), temp.getElement()) <= 0) {
				this.addBetween(newNode, this.header, temp);
				this.currentSize++;
				return true;
			}else {
				while(temp.getElement() != null) {
					temp = temp.getNext();
					Node<E> prevTemp = temp.getPrev();
					if(cmp.compare(newNode.getElement(), temp.getElement()) <= 0) {
						this.addBetween(newNode, prevTemp, temp);
						this.currentSize++;
						return true;
					}
				}
			}

		}
		return false;
	}

	@Override
	public int size() {
		return this.currentSize;
	}

	
	/*
	 * This remove method take sin a single parameter which represents the object that 
	 * should get removed from the structure.
	 * 
	 * The algorithm first checks of the object forms part of the structure and if not then returns false.
	 * Else, it uses an auxiliar method that returns a specific node, given a specific index
	 * and with it, it goes on to update the references of the node before that one and the one after it
	 * so the target node is left outside the structure and then another auxiliar method of the Node class is used to 
	 * get rid of all the data in the target node (clear())
	 * 
	 * The method returns true if the element was successfully removed and false otherwise.
	 */
	@Override
	public boolean remove(E obj) {
		if(!this.contains(obj)) {
			return false;
		}else {
			Node<E> ntr = this.getNode(this.firstIndex(obj));
			ntr.getPrev().setNext(ntr.getNext());
			ntr.getNext().setPrev(ntr.getPrev());
			ntr.clear();
			this.currentSize--;
			return true;
		}
	}

	
	/*
	 * This remove method takes in a single parameter which represents the index of the node that is to be removed.
	 * 
	 * The algorithm first checks that the entered index is within the range of the structure's size.
	 * Then it iterates through the structure until the iteration variable 'i' is equal to the given 'index' 
	 * and when that is true then the references are updated accordingly as to remove the target node.
	 * 
	 * The method returns true if the element was successfully removed and false otherwise.
	 */
	@Override
	public boolean remove(int index) {
		if(index < 0 || index >= this.size()) {
			throw new IndexOutOfBoundsException("index was: " + index + " and size was: " + this.size());
		}else {
			Node<E> temp = this.header;
			for(int i = 0; i<this.size(); i++) {
				temp = temp.getNext();
				if(i == index) {
					temp.getPrev().setNext(temp.getNext());
					temp.getNext().setPrev(temp.getPrev());
					temp.clear();
					this.currentSize--;
					return true;
				}
			}
		}
		return false;
	}

	
	/*
	 * This remove method takes in a single parameter that represents the object which is to be completely removed from the structure(all of its occurences)
	 * 
	 * The algorithm does a while loop that goes on until the method 'contains' returns false for the given object,
	 * and until then it uses the 'remove(E obj)' remove method to remove the copy of the object.
	 * 
	 * The method returns the amount of times the object was removed.
	 */
	@Override
	public int removeAll(E obj) {
		int itr = 0;
		while(this.remove(obj)) { itr++; };
		return itr;
	}

	@Override
	public E first() {
		return this.header.getNext().getElement();
	}

	@Override
	public E last() {
		return this.header.getPrev().getElement();
	}

	
	/*
	 * The get method takes in a single parameter that represents the index of the object which is to be returned
	 * 
	 *  The algorithm first checks if the entered index is within the range of the structure's size
	 *  if so then it goes on to iterate through the structure untill the index is found and then its value returned. 
	 */
	@Override
	public E get(int index) {
		if(index < 0 || index >= this.size()) {
			throw new IndexOutOfBoundsException();
		}else if(index == 0) {
			return this.header.getNext().getElement();
		}else {
			E etr = null;
			int i = 0;
			for(E e : this) {
				if(i == index) {
					etr = e;
					return etr;
				}
				else
					i++;
			}
			return etr;
		}
	}

	/*
	 * Auxiliar method to get the Node at a certain index
	 */
	public Node<E> getNode(int index) {
		if(index < 0 || index >= this.size()) {
			throw new IndexOutOfBoundsException();
		}else if(index == 0) {
			return this.header.getNext();
		}else {
			Node<E> temp = this.header;
			for(int i = 0; i<this.size(); i++) {
				temp = temp.getNext();
				if(i == index)
					return temp;
			}
		}
		throw new NoSuchElementException();
	}

	/*
	 * The clear method doesnt take any parameters
	 * 
	 * The algorithm simply uses a while loop that goes while the structure isnt empty and keeps removing the object at the index '0'.
	 */
	@Override
	public void clear() {
		while(!this.isEmpty()) {
			this.remove(0);
		}

	}

	/*
	 * The contains method takes in a single parameter which represents the object that is to be looked for inside the structure
	 * 
	 * The algorithm uses the 'firstIndex' method and it the integer returned by that method is greater or equal to 0 then it returns true.
	 * Otherwise it returns false.
	 */
	@Override
	public boolean contains(E e) {
		return this.firstIndex(e) >= 0;
	}

	@Override
	public boolean isEmpty() {
		return this.size() == 0;
	}

	/*
	 * The firstIndex method takes in a single parameter which represents the object which first occurrence index is to be returned.
	 * 
	 * The algorithm iterates through out the list until the element matches the element given as parameter and then returns the value of the 
	 * iteration variable at that time, if never found then it returns '-1'.
	 */
	@Override
	public int firstIndex(E e) {
		Node<E> temp = this.header;
		for(int i = 0; i<this.size(); i++) {
			temp = temp.getNext();
			if(cmp.compare(temp.getElement(), e) == 0)
				return i;
		}
		return -1;
	}

	/*
	 * The lastIndex method takes in a single parameter which represents the object which last occurrence index is to be returned.
	 * 
	 * The algorithm iterates through out the structure starting from the last position and goes down
	 * until the element matches the element given as parameter and then returns the value of the 
	 * iteration variable 'i' at that time, if never found then it returns '-1'.
	 */
	@Override
	public int lastIndex(E e) {
		Node<E> temp = this.header;
		for(int i = this.size()-1; i>=0; i--) {
			temp = temp.getPrev();
			if(cmp.compare(temp.getElement(), e) == 0)
				return i;
		}
		return -1;
	}

	/*
	 * Auxiliar method to add new Node between to existing Nodes.
	 */
	private void addBetween(Node<E> newN, Node<E> p , Node<E> n) {
		newN.setPrev(p);
		newN.setNext(n);
		p.setNext(newN);
		n.setPrev(newN);
	}

}