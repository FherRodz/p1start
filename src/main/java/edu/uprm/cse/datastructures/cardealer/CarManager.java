package edu.uprm.cse.datastructures.cardealer;

import java.util.Comparator;

import javax.ws.rs.*;
import javax.ws.rs.core.*;

import edu.uprm.cse.datastructures.cardealer.model.*;

import edu.uprm.cse.datastructures.cardealer.util.CircularSortedDoublyLinkedList;


/*
 * This class was mainly based on the REST API class from the link given 
 * inside the project's instructions.
 * 
 * It has two methods to 'get' the cars, one that returns all the cars in an array of Cars
 * and one that takes in a parameter of type 'Integer' that represents the ID of the car that is going to be returned by the method.
 * 
 * The 'add' method takes in one parameter of type <Car> which is the element thats going to be
 * stored in the server (using the SortedCircularDoublyLinkedList as the structure to store the Cars) 
 * and once added it produces the 201, or 'OK' response and sends it to the server.
 * 
 * The 'update' method takes in two parameters, one of type 'Integer' that represents the ID of the car that is requested to be updated
 * and one of type 'Car' that that represents the new car that the old one is going to be updated into
 * and then returns an 'OK' response and sends it to the server. 
 * 
 * The final method is the 'delete' method that also takes in one parameter of type 'Integer'
 * which represents the ID of the car that is to be taken out of the list
 * and then produces either an 'OK' or and '404' response depending on if the requested ID exists or not,
 * and then sends it to the server.
 * 
 * The 'NotFoundException and the JsonError classes where taken from the REST API site in order to use them in this API.
 */
@Path("/cars")
public class CarManager {
	private final Comparator<Car> cmp = new CarComparator();
	private final CircularSortedDoublyLinkedList<Car> cList = CarList.getInstance();
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Car[] getAllCars() {
		Car[] atr = new Car[cList.size()];
		int i =0;
		for(Car c : cList) {
			atr[i] = c;
			i++;
		}
		return atr;
	}

	@GET
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Car getCar(@PathParam("id") long id) {
		Car ctr;
		for(Car c : cList) {
			if(c.getCarId() == id) {
				ctr = c;
				return ctr;
			}
		}
		throw new WebApplicationException (404);
	}

	@POST
	@Path("/add")
	@Produces(MediaType.APPLICATION_JSON)
	public Response addCar(Car car) {
		cList.add(car);
		return Response.status(201).build();
	}

	@PUT
	@Path("/{id}/update")
	@Produces(MediaType.APPLICATION_JSON)
	public Response updateCar(Car car, @PathParam("id") long id) {
		int i = 0;
		for(Car c : cList) {
			if(c.getCarId() == id){
				cList.remove(i);
				cList.add(car);
				return Response.status(Response.Status.OK).build();
			}
			i++;
		}
		
		return Response.status(Response.Status.NOT_FOUND).build();      
		
	}

	@DELETE
	@Path("/{id}/delete")
	public Response deleteCar(@PathParam("id") long id) {
		int i = 0;
		boolean found = false;
		for(Car c : cList) {
			if(c.getCarId() == id){
				cList.remove(i);
				found = !found;
				return Response.status(Response.Status.OK).build();
			}
			i++;
		}
		if(!found)
			throw new NotFoundException(new JsonError("Error", "Customer " + id + " not found"));
    	return Response.status(404).build();

	}

}