package edu.uprm.cse.datastructures.cardealer.model;

import java.util.Comparator;


/*
 * Comparator class that holds the 'compare' method to compare two objects of type 'Car'.
 */
public class CarComparator implements Comparator<Car>{

	
	/*
	 * The compare method take sin two parameters of type 'Car' which represent the two cars to be compared
	 * 
	 * The algorithm compares the two car's 'brand', 'carModel' and 'carModelOption' String fields to determine 
	 * if the car 'o1' is greater, lesser or equal to car o2.
	 * 
	 * It returns a negative integer if car 'o1' is lesser than car 'o2', a positive integer if its greater and a '0' if they are equal.
	 */
	@Override
	public int compare(Car o1, Car o2) {

		if(o1.getCarBrand().compareTo(o2.getCarBrand()) == 0 && o1.getCarModel().compareTo(o2.getCarModel()) == 0) {
			return o1.getCarModelOption().compareTo(o2.getCarModelOption());
		}else if(o1.getCarBrand().compareTo(o2.getCarBrand()) == 0) {
			return o1.getCarModel().compareTo(o2.getCarModel());
		}else {
			return o1.getCarBrand().compareTo(o2.getCarBrand());
		}
	}
	
}

